/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10 10:17
 */
//充电桩协议测试工具
package main

import (
	"HpTest/TcpPack"
	"HpTest/base"
	"fmt"
	"sync"
)

var (
	//全局配置
	Config *base.Config = base.C
	//主工作线程 等待
	workerMain sync.WaitGroup
)

func main() {

	defer func() {
		base.Log.Emergency("程序发生异常")
		if err := recover(); err != nil {
			base.Log.Emergency("异常错误信息：" + fmt.Sprintf("%s", err))
		}
		base.Log.Emergency("程序自动异常恢复")
	}()

	base.Log.Info("充电桩通讯协议调试工具2019 v1.04.08")

	OutConfigInfo()

	//开启socket监听
	workerMain.Add(1)
	go SocketServer()
	//充电桩线程检测
	go TcpPack.ChargeConnCheck()

	workerMain.Wait()
}
//输出配置信息
func OutConfigInfo(){
	base.Log.Info("初始化充电桩列表：")
	num:=0
	for _,v:=range TcpPack.Piles{
		fmt.Printf("%s",v)
		if num< len(TcpPack.Piles)-1{
			fmt.Printf(",")
		}
		num++
	}
	fmt.Println()

	base.Log.Info("设置的充电金额：%d元",base.C.Charge.Money/100)
	base.Log.Info("默认充电时间：%d分钟",base.C.Charge.Time)
}





