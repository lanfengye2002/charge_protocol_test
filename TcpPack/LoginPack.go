/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"encoding/binary"
	"base/common"
	"io"
)

//登录数据包定义

type LoginPack struct {
	LoginTime string   //登录时间 BCD码 YYYY:MM:DD:HH:MM:SS
	Type      int8     //桩的类型 0-落地式交流充电桩 4-单枪一体式充电桩 5-双枪一体式充电桩 6-四枪一体式充电桩 7-单枪厢式终端 8-双枪厢式终端
	Reserve   int32    //桩登录预留信息 4字节整型数
	Version   int32    //桩版本号 4字节整型数
	Retain    [16]byte //保留信息 暂时固定为0 桩上可能会不上传这16个字节保留数据
}

//登录回应
type LoginReplyPack struct {
	Confirm int8 //登录确认标志 0-主站未确认 1-主站确认登录
	Version int32 //后台通讯软件版本 整型数
}

//登录回应包编码
func (p *LoginReplyPack) Pack(writer io.Writer) error{
	var err error

	err = binary.Write(writer, binary.LittleEndian, &p.Confirm)
	err = binary.Write(writer, binary.LittleEndian, &p.Version)

	return err
}

//登录请求包解码
func (p *LoginPack) Unpack(reader io.Reader) error {
	var err error

	var bcd [7]byte
	err = binary.Read(reader, binary.LittleEndian, &bcd)

	if(err!=nil){
		return err
	}

	p.LoginTime = common.Bcd2time(bcd)

	err = binary.Read(reader, binary.LittleEndian, &p.LoginTime)
	err = binary.Read(reader, binary.LittleEndian, &p.Type)
	err = binary.Read(reader, binary.LittleEndian, &p.Reserve)
	err = binary.Read(reader, binary.LittleEndian, &p.Version)
	err = binary.Read(reader, binary.LittleEndian, &p.Retain)

	return err
}
