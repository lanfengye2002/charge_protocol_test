/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
//协议流程测试包，对全部指令按照顺序进行下发和测试
package TcpPack

import (
	"HpTest/data"
	"bytes"
	"time"
)

//测试处理 按照流程对指定指令进行测试
func TestHandle(conn *HpCharge) {

}

//主动下发费率检测  //进行主动下发费率协议的检测 登录成功后120s主动下发 并检测在5秒内是否进行回复
func Test0x05(conn *HpCharge) {
	time.Sleep(time.Duration(120) * time.Second)

	Log.Info("[%s][%d][服务端主动下发浮点费率][登录成功后150s时自动下发一次]：", conn.ConnId, conn.DeviceId)

	////获取按照时间段排序好的计费模型slice
	bill_slice := data.GetBillingData()

	//获取浮点费率
	rate := &RatePack{
	}

	buf := new(bytes.Buffer)
	rate.Pack(buf, bill_slice)

	r1:=&WriteDataPack{
		Count:20,
		Data:buf.Bytes()[16:],
		RecordID:1234,
	}
	b1 := new(bytes.Buffer)
	r1.Pack(b1)

	f := PackAll(conn.DeviceId, "05", "0", "0", 0, b1.Bytes())

	//将浮点费率结果发送给充电桩
	conn.WriteData(f)
	//检测0x05回复
	go func(conn *HpCharge) {
		//发送对时报文后 5s内未收到回复则输出错误
		select {
		case <-time.After(time.Duration(5) * time.Second):
			Log.Error("[%s][%d][监控中心写数据0x05失败][未在规定时间内回复]", conn.ConnId, conn.DeviceId)
		case <-conn.chan0x05:
			Log.Info("[%s][%d][通过：监控中心写数据0x05成功][回复成功]", conn.ConnId, conn.DeviceId)
		case <-conn.ctx.Done():
			Log.Info("[%s][%d][监控中心写数据0x05检测终止][]", conn.ConnId, conn.DeviceId)
		}
	}(conn)

}

//进行插枪后的主动上传刷卡报文的检测
func Test0x30(conn *HpCharge) {
	//发送报文后 5s内未收到回复则输出错误
	select {
	case <-time.After(time.Duration(10) * time.Second):
		Log.Error("[%s][%d][主动上传刷卡请求0x30失败][未在规定时间内上传]", conn.ConnId, conn.DeviceId)
	case <-conn.chan0x30:
		Log.Info("[%s][%d][通过：主动上传刷卡请求0x30成功][接收到插枪vin报文]", conn.ConnId, conn.DeviceId)
	case <-conn.ctx.Done():
		Log.Info("[%s][%d][主动上传刷卡请求0x30检测终止][]", conn.ConnId, conn.DeviceId)
	}
}

//进行插枪后30s进行启动充电指令下发
func Test0x10start(conn *HpCharge) {
	time.Sleep(time.Duration(30) * time.Second)
	conn.ChargeStart(config.Charge.GunId, "15940442002", int32(config.Charge.Money), 1)
	go func() {
		select {
		case <-time.After(time.Duration(10) * time.Second):
			Log.Error("[%s][%d][启动充电命令0x10失败][未在规定时间内回复]", conn.ConnId, conn.DeviceId)
		case <-conn.chan0x10start:
			Log.Info("[%s][%d][通过：启动充电命令0x10成功][规定时间内收到回复]", conn.ConnId, conn.DeviceId)
		case <-conn.ctx.Done():
			Log.Info("[%s][%d][启动充电命令0x10检测终止][]", conn.ConnId, conn.DeviceId)
		}
	}()

	//下发启动充电后30s内是否收到0x45回复
	go func() {
		select {
		case <-time.After(time.Duration(30) * time.Second):
			Log.Error("[%s][%d][充电桩上报充电命令0x45失败][未在规定时间内回复]", conn.ConnId, conn.DeviceId)
		case <-conn.chan0x45:
			Log.Info("[%s][%d][通过：充电桩上报充电命令0x45成功][规定时间内收到回复]", conn.ConnId, conn.DeviceId)
		case <-conn.ctx.Done():
			Log.Info("[%s][%d][充电桩上报充电命令0x45检测终止][]", conn.ConnId, conn.DeviceId)
		}
	}()

}

func Test0x10stop(conn *HpCharge){
	time.Sleep(config.Charge.Time * time.Minute)
	conn.ChargeStop(config.Charge.GunId,"15940442002",0,1)
	//下发停止充电后5s内是否收到回复
	go func() {
		select {
		case <-time.After(time.Duration(5) * time.Second):
			Log.Error("[%s][%d][失败：停止充电命令0x10失败][未在规定时间内回复]", conn.ConnId, conn.DeviceId)
		case <-conn.chan0x10stop:
			Log.Info("[%s][%d][通过：停止充电命令0x10成功][规定时间内收到回复]", conn.ConnId, conn.DeviceId)
		case <-conn.ctx.Done():
			Log.Info("[%s][%d][停止充电命令0x10检测终止][]", conn.ConnId, conn.DeviceId)
		}
	}()

	go func() {
		select {
		case <-time.After(time.Duration(10) * time.Second):
			Log.Error("[%s][%d][失败：主动上传历史数据0x06失败][未在规定时间内上传账单]", conn.ConnId, conn.DeviceId)
		case <-conn.chan0x06:
			Log.Info("[%s][%d][通过：主动上传历史数据0x06成功][规定时间内上传账单]", conn.ConnId, conn.DeviceId)
		case <-conn.ctx.Done():
			Log.Info("[%s][%d][主动上传历史数据0x06成功检测终止][]", conn.ConnId, conn.DeviceId)
		}
	}()


}
