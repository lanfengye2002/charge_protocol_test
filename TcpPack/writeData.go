/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

//写入数据充电桩回复
func (p *PackBase) writeDataReply(conn *HpCharge){
	Log.Info("[%s][%d][充电桩回复监控中心写数据0x05下发计费模型][成功]",conn.ConnId,p.DeviceId)
	conn.chan0x05<-1
}
