/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"bytes"
)

func (p *PackBase) cardRequest(conn *HpCharge) {
	//业务数据解码
	info := new(CardPack)
	info.Unpack(bytes.NewReader(p.Data))
	Log.Info("[%s][%d-%s][刷卡业务数据][%+v]", conn.ConnId,p.DeviceId,p.GunId,info)

	conn.chan0x30<-1

	//刷卡回复
	write := &CardReplyPack{
		CardIdByte: info.CarIdByte,
		Money:      0,
		CardState:  4,
		SerialNo:   0,
		MoneyMin:   10,
	}

	buf := new(bytes.Buffer)
	write.Pack(buf)

	f := PackAll(p.DeviceId, p.Direct, "0", p.GunId, 1, buf.Bytes())

	conn.WriteData(f)

}
