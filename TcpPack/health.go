/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"bytes"
	"time"
)

//心跳业务处理


//接收心跳处理
func (p *PackBase) health(conn *HpCharge){

	//业务数据解码
	info := new(HealthPack)
	info.Unpack(bytes.NewReader(p.Data))

	Log.Info("[%s][%d][接收到心跳数据][%+v]", conn.ConnId,conn.DeviceId,info)

	conn.HealthLastTime=time.Now().Unix()

	conn.SetConnDeviceID(p.DeviceId)
	conn.chanHealth<-1

	//进行回复心跳
	//心跳返回数据
	write := &HealthReplyPack{
		Confirm: 1,
	}

	buf := new(bytes.Buffer)
	write.Pack(buf)

	f := PackAll(p.DeviceId, p.Direct, "0", p.GunId, 1, buf.Bytes())

	conn.WriteData(f)
}