/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"fmt"
	"bytes"
)

//主站对充电桩采集实时数据后 充电桩回复数据
func (p *PackBase) readRealTimeReply(conn *HpCharge) {
	////业务数据解码
	//info := new(RealTimePack)
	//info.Unpack(bytes.NewReader(p.Data))

	fmt.Println(fmt.Sprintf("充电桩[%d]回复服务端主动采集实时数据：%s", p.DeviceId, p.Data))

	//fmt.Println("写入通道数据错误：", conn.WriteChanData(fmt.Sprintf("09-0"), p.Data))
}

//主站采集实时数据示例
func (conn *HpCharge) GetReadRealTime(gun_id string) (*RealTimePack,error) {
	//采集实时数据结构体
	login := &ReadRealTimePack{
	}

	buf := new(bytes.Buffer)
	login.Pack(buf)

	f := PackAll(conn.DeviceId, "09", "0", gun_id, 0, buf.Bytes())

	conn.WriteData(f)

	//b, err := conn.ReadChanData(fmt.Sprintf("09-%s", gun_id))
	//fmt.Println("通道返回数据：", b)
	//fmt.Println("通道读取数据错误：", err)
	//if err != nil {
	//	return nil,err
	//}

	//业务数据解码
	info := new(RealTimePack)
	//info.Unpack(bytes.NewReader(b))

	return info,nil
}
