/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"HpTest/data"
	"bytes"
)

//充电桩获取浮点费率

func (p *PackBase) getRate(conn *HpCharge,direct string){

	Log.Info("[%s][%d][充电桩获取浮点费率][指令：%s]：",conn.ConnId,conn.DeviceId,direct)
	conn.chan0x41<-1

	////获取按照时间段排序好的计费模型slice
	bill_slice:=data.GetBillingData()


	//获取浮点费率
	rate := &RatePack{
	}

	buf := new(bytes.Buffer)
	rate.Pack(buf,bill_slice)

	f := PackAll(p.DeviceId, direct, "0", "0", 0, buf.Bytes())

	//fmt.Println(hex.EncodeToString(f))

	//将浮点费率结果发送给充电桩
	conn.WriteData(f)
}



func (p *PackBase) getRate0x05(conn *HpCharge,direct string){

	Log.Info("[%s][%d][服务端下发浮点费率][指令：%s]：",conn.ConnId,conn.DeviceId,direct)

	////获取按照时间段排序好的计费模型slice
	bill_slice:=data.GetBillingData()


	//获取浮点费率
	rate := &RatePack{
	}

	buf := new(bytes.Buffer)
	rate.Pack(buf,bill_slice)

	r1:=&WriteDataPack{
		Count:20,
		Data:buf.Bytes()[16:],
		RecordID:1234,
	}
	b1 := new(bytes.Buffer)
	r1.Pack(b1)

	f := PackAll(p.DeviceId, direct, "0", "0", 0, buf.Bytes())

	//fmt.Println(hex.EncodeToString(f))

	//将浮点费率结果发送给充电桩
	conn.WriteData(f)
}
