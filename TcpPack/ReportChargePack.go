/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"io"
	"encoding/binary"
	"strings"
)

//充电桩上报充电命令

//充电桩上报充电命令包
type ReportChargePack struct {
	Marking int8   //启停标识 0-成功 1-失败
	UserId  string //用户号或者手机号，字符串格式，20字节，小于20字节后面补0
}

//服务器回应
type ReportChargeReplyPack struct {
	Confirm int8   //确认标志 0-确认
	UserId  string //用户号或者为手机号 字符串格式，20字节，小于20字节后面补0
}

func (p *ReportChargeReplyPack) Pack(writer io.Writer) error {
	var err error
	err = binary.Write(writer, binary.LittleEndian, &p.Confirm)

	var userId [20]byte
	b := []byte(p.UserId)

	for i := 0; i < len(b); i++ {
		userId[i] = b[i]
	}

	err = binary.Write(writer, binary.LittleEndian, &userId)

	return err
}

//解码
func (p *ReportChargePack) Unpack(reader io.Reader) error {
	var err error

	//启停标识
	err = binary.Read(reader, binary.LittleEndian, &p.Marking)

	//用户号
	var userId [20]byte
	err = binary.Read(reader, binary.LittleEndian, &userId)

	p.UserId = strings.ReplaceAll(string(userId[:]),"\x00","")

	return err
}
