/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"HpTest/base"
	"strings"
)

var (
	//包全局配置
	config = base.C
	Log    = base.Log
	//充电桩编号数据
	Piles map[string]string
	//通道数据 用于处理需要同步返回的数据
)

func init(){
	l:=strings.Split(config.Pile.List,",")
	Piles=make(map[string]string)
	for _,v:=range l{
		Piles[v]=v
	}
}
