/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"io"
	"encoding/binary"
	"base/common"
)

//对时数据包定义

//对时包
type TimePack struct{
	Time string //时间 2018-11-01 11:01:03 会自动转换成bcd码字节
}

//对时回复包
type TimeReplyPack struct {
	Confirm int8 //确认标志
}

//对时包编码
func (p *TimePack) Pack(writer io.Writer) error{
	var err error

	bcd:=common.Time2bcd(p.Time)

	err = binary.Write(writer, binary.LittleEndian, &bcd)

	return err
}

//对时回应包解码
func (p *TimeReplyPack) Unpack(reader io.Reader) error{
	var err error

	err = binary.Read(reader, binary.LittleEndian, &p.Confirm)

	return err
}