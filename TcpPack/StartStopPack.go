/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"encoding/binary"
	"io"
	"strings"
)

//控制启停帧数据包

//启停数据包
type StartStopPack struct {
	Marking int8 //启停标示 1-启动 2-停止
	UserId string //用户号或者手机号，字符串格式，20字节，小于20字节后面补0
	Money int32 //余额 int整型数，单位为分
	OrderId uint32 //后台交易流水号 int无符号整型数 启动充电时后台分配的唯一流水号
	Mode int32 //充电模式 0-自动充电 充满 1-金额模式 2-时间模式 3-电量模式
	Parameter int32 //充电参数 金额模式-单位分 时间模式-秒 电量单位-0.01kw.H
	Retain [4]byte //保留 固定为0
}

//启停数据包
type StartStopReplyPack struct {
	Confirm int8 //启停确认标志 0-启动成功 1-启动失败 2-停止成功 3-停止失败
	UserId string //用户号或者手机号，字符串格式，20字节，小于20字节后面补0
	WarnPoint int32 //告警点
	WarnCause int32 //告警点原因
	OrderId uint32 //后台交易流水号 int无符号整型数 启动充电时后台分配的唯一流水号
}

func (p *StartStopReplyPack) Unpack(reader io.Reader) error{
	var err error

	err = binary.Read(reader, binary.LittleEndian, &p.Confirm)

	//用户号
	var userId [20]byte
	err = binary.Read(reader, binary.LittleEndian,&userId)

	p.UserId=strings.ReplaceAll(string(userId[:]),"\x00","")

	err = binary.Read(reader, binary.LittleEndian,&p.WarnPoint)
	err = binary.Read(reader, binary.LittleEndian,&p.WarnCause)
	err = binary.Read(reader, binary.LittleEndian,&p.OrderId)

	return err
}



//编码
func (p *StartStopPack) Pack(writer io.Writer) error{
	var err error
	err = binary.Write(writer, binary.LittleEndian, &p.Marking)

	var userId [20]byte
	b:=[]byte(p.UserId)

	for i:=0;i<len(b) ; i++ {
		userId[i]=b[i]
	}

	err = binary.Write(writer, binary.LittleEndian, &userId)

	err = binary.Write(writer, binary.LittleEndian, &p.Money)
	err = binary.Write(writer, binary.LittleEndian, &p.OrderId)
	err = binary.Write(writer, binary.LittleEndian, &p.Mode)
	err = binary.Write(writer, binary.LittleEndian, &p.Parameter)
	err = binary.Write(writer, binary.LittleEndian, &p.Retain)

	return err
}
