/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"bytes"
	"sync"
)

//充电桩主动上传历史记录处理
func (p *PackBase) historyWrite(conn *HpCharge) {
	var wait sync.WaitGroup
	//业务数据解码
	info := new(HistoryPack)
	info.Unpack(bytes.NewReader(p.Data))
	Log.Info("[%s][%d-%s][充电桩主动上传历史账单0x06数据][%+v]", conn.ConnId, p.DeviceId, p.GunId, info)

	conn.chan0x06<-1

	//创建新的协程进行处理业务数据 不进行等待 立即给充电桩进行应答
	wait.Add(1)
	go info.HandleBill(p,&wait)

	//历史记录返回数据
	write := &HistoryReplyPack{
		Count:     info.Count,
		RecordID:  info.RecordID,
		StorageID: info.StorageID,
		Confirm:   0,
	}

	buf := new(bytes.Buffer)
	write.Pack(buf)

	f := PackAll(p.DeviceId, p.Direct, "0", p.GunId, 2, buf.Bytes())

	conn.WriteData(f)
	wait.Wait()
}

//处理结账方法
func (p *HistoryPack) HandleBill(baseData *PackBase,wait *sync.WaitGroup) {

	if int(p.SumMoney*100)>config.Charge.Money{
		Log.Error("[][%d-%s][充电桩上报历史账单数据0x06错误][充电总金额超过设置金额]",baseData.DeviceId,baseData.GunId)
	}

	if p.ChargeMoney+p.ServiceMoney!=p.SumMoney{
		Log.Error("[][%d-%s][充电桩上报历史账单数据0x06错误][总金额不等于充电金额与服务费之和]",baseData.DeviceId,baseData.GunId)
	}

	wait.Done()
}
