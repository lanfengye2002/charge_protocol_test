/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"encoding/binary"
	"io"
	"encoding/hex"
)

//服务器采集实时数据
type ReadRealTimePack struct {
	//Count int16 //数据单元个数 正常为19个数据
}

func (p *ReadRealTimePack) Pack(writer io.Writer) error {
	var err error
	//采集的数据单元个数 固定为19个  一次全部采集回来

	var mark =[...]string{"0603","0604","0B01","0B02","0B03","0B04","0B05","0B06","0B07","0B08","0B09","0B0A","0B0B","0B0C","0B0D","0B0E","0B0F","0B10","0B11"}
	var count = int16(len(mark))

	err = binary.Write(writer, binary.LittleEndian, &count)
	//指令写入临时数据
	var d []byte
	var a [2]byte

	//循环写入全部指令标识
	for _,v:=range mark{
		d, err = hex.DecodeString(v)
		//指令标识小端序
		a[0],a[1]=d[1],d[0]
		err = binary.Write(writer, binary.LittleEndian, &a)
	}

	//fmt.Println(writer)

	/*fmt.Println(writer)

	d, err = hex.DecodeString("0406")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("010B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("020B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("030B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("040B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("050B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("060B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("070B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("080B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("090B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("0A0B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("0B0B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("0C0B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("0D0B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("0E0B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("0F0B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("100B")
	err = binary.Write(writer, binary.LittleEndian, &d)

	d, err = hex.DecodeString("110B")
	err = binary.Write(writer, binary.LittleEndian, &d)*/

	return err
}
