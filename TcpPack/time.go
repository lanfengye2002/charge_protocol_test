/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import "bytes"

//对时响应回复
func (p *PackBase) timeRespond(conn *HpCharge) {

	//业务数据解码
	info := new(TimeReplyPack)
	info.Unpack(bytes.NewReader(p.Data))

	conn.chanTime<-1

	if info.Confirm==1{
		Log.Info("[%s][%d][充电桩回复][确认对时]", conn.ConnId,p.DeviceId)
		return
	}

	Log.Info("[%s][%d][充电桩回复][对时失败]", conn.ConnId,p.DeviceId)
}