/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"io"
	"encoding/binary"
)

//心跳数据包定义

//心跳包结构体
type HealthPack struct {
	Heart   int8  //7 心跳间隔 默认30s
	RunTime int32 //8 桩从启动到当前的秒数
}

//心跳回应包
type HealthReplyPack struct {
	Confirm int8 //心跳确认标志 1-正确 0-处理错误
}

//基础包 编码打包
func (p *HealthReplyPack) Pack(writer io.Writer) error {
	var err error

	//确认标志
	err = binary.Write(writer, binary.LittleEndian, &p.Confirm)

	return err
}

func (p *HealthPack) Unpack(reader io.Reader) error {
	var err error
	//7 心跳间隔
	err = binary.Read(reader, binary.LittleEndian, &p.Heart)
	//8 桩运行时间 秒
	err = binary.Read(reader, binary.LittleEndian, &p.RunTime)
	return err
}
