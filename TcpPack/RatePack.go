/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"HpTest/data"
	"encoding/binary"
	"encoding/hex"
	"io"
	"strconv"
	"strings"
)

//充电桩获取浮点费率TCP包规则

//浮点费率规则包
type RatePack struct {
}

func (p *RatePack) Pack(writer io.Writer, bill_slice *data.BtSlice) error {
	var err error
	//数据单元个数
	var length = int16(20)

	//价格数据标识
	price_mark := [...]string{"1500", "1600", "1700", "1800", "1900", "1A00", "1B00", "1C00", "1D00", "1E00"}
	var mark [2]byte
	////时间段标识
	time_slot_mark := [...]string{"1F00", "2000", "2100", "2200", "2300", "2400", "2500", "2600", "2700", "2800"}
	////价格数据单元长度
	price_len := int8(5)
	////时间段数据单元长度
	time_len := int8(2)

	var time_hour_len int8
	var time_minute_len int8

	//写入数据单元个数
	err = binary.Write(writer, binary.LittleEndian, &length)

	//遍历写入各阶段价格
	for k, v := range price_mark {
		//电费价格数据
		var elty_price int16
		//服务费价格数据
		var service_price int16
		//阶段标识
		var slot_type int8

		//单位统一为0.01 带两位小数的整型数值
		elty_price = int16((*bill_slice)[k].Elty_price * 100)
		service_price = int16((*bill_slice)[k].Service_price * 100)
		slot_type = (*bill_slice)[k].Slot_type
		a, _ := hex.DecodeString(v)
		mark[0], mark[1] = a[0], a[1]
		err = binary.Write(writer, binary.LittleEndian, &mark)
		err = binary.Write(writer, binary.LittleEndian, &price_len)
		err = binary.Write(writer, binary.LittleEndian, &slot_type)
		err = binary.Write(writer, binary.LittleEndian, &elty_price)
		err = binary.Write(writer, binary.LittleEndian, &service_price)
	}

	//遍历写入时间段数据 只有开始时间 00:00-05:00
	for k, v := range time_slot_mark {
		a, _ := hex.DecodeString(v)
		mark[0], mark[1] = a[0], a[1]

		time_start := strings.Split((*bill_slice)[k].Time, "-")
		//fmt.Println(time_start)

		time_s := strings.Split(time_start[0], ":")

		h, _ := strconv.Atoi(time_s[0])
		time_hour_len = int8(h)

		m, _ := strconv.Atoi(time_s[1])
		time_minute_len = int8(m)

		err = binary.Write(writer, binary.LittleEndian, &mark)
		err = binary.Write(writer, binary.LittleEndian, &time_len)
		err = binary.Write(writer, binary.LittleEndian, &time_hour_len)
		err = binary.Write(writer, binary.LittleEndian, &time_minute_len)

		//if k == len(*bill_slice)-1 {
		//	a, _ := hex.DecodeString(time_slot_mark[k+1])
		//	mark[0], mark[1] = a[0], a[1]
		//
		//	time_s := strings.Split(time_start[1], ":")
		//
		//	h, _ := strconv.Atoi(time_s[0])
		//	time_hour_len = int8(h)
		//
		//	m, _ := strconv.Atoi(time_s[1])
		//	time_minute_len = int8(m)
		//
		//	err = binary.Write(writer, binary.LittleEndian, &mark)
		//	err = binary.Write(writer, binary.LittleEndian, &time_len)
		//	err = binary.Write(writer, binary.LittleEndian, &time_hour_len)
		//	err = binary.Write(writer, binary.LittleEndian, &time_minute_len)
		//}
	}

	return err
}
