/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"bytes"
)

//充电桩主动上报充电充电
func (p *PackBase) reportCharge(conn *HpCharge){
	//业务数据解码
	info := new(ReportChargePack)
	info.Unpack(bytes.NewReader(p.Data))

	Log.Info("[%s][%d-%s][充电桩主动上报充电状态数据][%+v]",conn.ConnId,p.DeviceId,p.GunId,info)

	conn.chan0x45<-1

	//确认回复
	write := &ReportChargeReplyPack{
		Confirm:0,
		UserId:info.UserId,
	}

	buf := new(bytes.Buffer)
	write.Pack(buf)

	f := PackAll(p.DeviceId, p.Direct, "0", p.GunId, 1, buf.Bytes())

	conn.WriteData(f)
}
