/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"bytes"
)

//充电桩主动上传告警数据写入
func (p *PackBase) warnWrite(conn *HpCharge) {
	//业务数据解码
	info := new(WarnPack)
	info.Unpack(bytes.NewReader(p.Data))

	Log.Info("[%s][%d][充电桩上报告警数据][%+v]",conn.ConnId,p.DeviceId,info)

	//layout:="2006-01-02 15:04:05"
	//t1, _ := time.ParseInLocation(layout, info.BeginTime,time.Local)
	//t2, _ := time.ParseInLocation(layout, info.StopTime,time.Local)

//	//充电桩主动上传告警数据写入
//	data.Mysql.Insert(`insert into yx_warn_record
//(device_id,gun_id,point,cause,begin_time,end_time,duration,if_charge,if_upload,storage_id,storage_record,create_time)
//values (?,?,?,?,?,?,?,?,?,?,?,?)`,
//		p.DeviceId, p.GunId,
//		info.Point, info.Cause,
//		t1.Unix(), t2.Unix(), info.Duration,
//		info.IfCharge, info.IfUpload,
//		info.StorageID, info.StorageIDD, time.Now().Unix())

	//告警数据收到确认回复
	write := &WarnReplyPack{
		Count:      info.Count,
		StorageID:  info.StorageID,
		StorageIDD: info.StorageIDD,
	}

	buf := new(bytes.Buffer)
	write.Pack(buf)

	f := PackAll(p.DeviceId, p.Direct, "0", p.GunId, 2, buf.Bytes())

	conn.WriteData(f)
}
