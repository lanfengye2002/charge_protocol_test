/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10 10:17
 */
package TcpPack

import (
	"bytes"
)

//主动上传实时数据处理
func (p *PackBase) realTime(conn *HpCharge){

	//业务数据解码
	info := new(RealTimePack)
	info.Unpack(bytes.NewReader(p.Data))

	Log.Info("[%s][%d-%s][充电桩主动上传实时数据][%+v]",conn.ConnId,p.DeviceId,p.GunId,info)

	conn.chanReal<-1

	if info.DeviceStatus==0{
		conn.readyStart=false
		conn.readyCard=false
		conn.readyStarted=false
	}

	if info.DeviceStatus==1{
		if !conn.readyCard{
			//插枪后 监测是否主动上报刷卡请求
			go Test0x30(conn)
			conn.readyCard=true
		}
		if !conn.readyStart{
			//插枪后 30s 自动开始充电指令
			go Test0x10start(conn)
			conn.readyStart=true
		}

	}
	//充电中 后 5分钟下发停止充电指令
	if info.DeviceStatus==2 && !conn.readyStarted{
		go Test0x10stop(conn)
		conn.readyStarted=true
	}

	//实时数据确认收到
	write := &RealTimeReplyPack{
		Count:info.Count,
		Confirm: 1,
	}

	buf := new(bytes.Buffer)
	write.Pack(buf)

	f := PackAll(p.DeviceId, p.Direct, "0", p.GunId, 2, buf.Bytes())

	conn.WriteData(f)
}