/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"io"
	"encoding/binary"
	"base/common"
)

//告警数据包定义


//告警记录包定义
type WarnPack struct{
	Count int16 //数据单元个数
	Point int32 //告警点
	Cause int32 //告警原因
	BeginTime string //告警开始时间 BCD 7字节
	StopTime string //告警结束时间 BCD 7字节
	Duration int32 //持续时间 单位秒
	IfCharge int8 //是否影响充电 0-无影响 1-有影响
	IfUpload int8 //是否上传主站 0-无 1-有
	StorageID int32 //记录存储号
	StorageIDD int32 //记录存储号

}

//告警记录回复包定义
type WarnReplyPack struct{
	Count int16 //数据单元个数
	StorageID int32 //记录存储号
	StorageIDD int32 //记录存储号
}

func (p *WarnReplyPack) Pack(writer io.Writer) error{
	var err error
	err = binary.Write(writer, binary.LittleEndian, &p.Count)
	err = binary.Write(writer, binary.LittleEndian, &p.StorageID)
	err = binary.Write(writer, binary.LittleEndian, &p.StorageIDD)

	return err
}

func (p *WarnPack) Unpack(reader io.Reader) error{
	var err error
	//数据单元个数
	err = binary.Read(reader, binary.LittleEndian, &p.Count)

	var b [2]byte //数据单元标识
	var l int8 //数据单元长度
	var bcd [7]byte //bcd 7字节数据
	//单元1
	err = binary.Read(reader, binary.LittleEndian, &b)
	err = binary.Read(reader, binary.LittleEndian, &l)
	err = binary.Read(reader, binary.LittleEndian, &p.Point)

	//单元2
	err = binary.Read(reader, binary.LittleEndian, &b)
	err = binary.Read(reader, binary.LittleEndian, &l)
	err = binary.Read(reader, binary.LittleEndian, &p.Cause)

	//单元3
	err = binary.Read(reader, binary.LittleEndian, &b)
	err = binary.Read(reader, binary.LittleEndian, &l)
	err = binary.Read(reader, binary.LittleEndian, &bcd)
	p.BeginTime=common.Bcd2time(bcd)

	//单元4
	err = binary.Read(reader, binary.LittleEndian, &b)
	err = binary.Read(reader, binary.LittleEndian, &l)
	err = binary.Read(reader, binary.LittleEndian, &bcd)
	p.StopTime=common.Bcd2time(bcd)

	//单元5
	err = binary.Read(reader, binary.LittleEndian, &b)
	err = binary.Read(reader, binary.LittleEndian, &l)
	err = binary.Read(reader, binary.LittleEndian, &p.Duration)

	//单元6
	err = binary.Read(reader, binary.LittleEndian, &b)
	err = binary.Read(reader, binary.LittleEndian, &l)
	err = binary.Read(reader, binary.LittleEndian, &p.IfCharge)

	//单元7
	err = binary.Read(reader, binary.LittleEndian, &b)
	err = binary.Read(reader, binary.LittleEndian, &l)
	err = binary.Read(reader, binary.LittleEndian, &p.IfUpload)

	//单元8
	err = binary.Read(reader, binary.LittleEndian, &b)
	err = binary.Read(reader, binary.LittleEndian, &l)
	err = binary.Read(reader, binary.LittleEndian, &p.StorageID)

	//单元9
	err = binary.Read(reader, binary.LittleEndian, &b)
	err = binary.Read(reader, binary.LittleEndian, &l)
	err = binary.Read(reader, binary.LittleEndian, &p.StorageIDD)

	return err
}




