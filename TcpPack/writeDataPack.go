/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-12 13:29
 */
package TcpPack

import (
	"encoding/binary"
	"io"
)

//写数据指令包
type WriteDataPack struct {
	Count int16 //传输单元个数
	Data []byte //向下写入数据
	RecordID int32 //记录号
}


func (t *WriteDataPack) Pack(writer io.Writer)(err error) {
	err = binary.Write(writer, binary.LittleEndian, &t.Count)
	err = binary.Write(writer, binary.LittleEndian, &t.Data)
	err = binary.Write(writer, binary.LittleEndian, &t.RecordID)
	return
}
