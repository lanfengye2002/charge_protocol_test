/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package TcpPack

import (
	"bytes"
)

//启停控制逻辑

//充电桩启停回复
func (p *PackBase) startStopReply(conn *HpCharge){
	//业务数据解码
	info := new(StartStopReplyPack)
	info.Unpack(bytes.NewReader(p.Data))

	Log.Info("[%s][%d-%s][充电桩启停回复][%+v]",conn.ConnId,p.DeviceId,p.GunId,info)

	if info.Confirm==0 || info.Confirm==1{
		conn.chan0x10start<-1
	}

	if info.Confirm==2 || info.Confirm==3{
		conn.chan0x10stop<-1
	}
}


//充电桩开始充电命令
func (conn *HpCharge) ChargeStart(gun_id string,UserId string,Money int32,OrderId uint32) (error){
	da := &StartStopPack{
		Marking:   1,
		UserId:    UserId,
		Money:     Money,
		OrderId:   OrderId,
		Mode:      1,
		Parameter: Money,
	}
	buf := new(bytes.Buffer)
	da.Pack(buf)

	f := PackAll(conn.DeviceId, "10", "0", gun_id, 0, buf.Bytes())

	Log.Info("[%s][%d-%s][启动充电][%+v]",conn.ConnId,conn.DeviceId,gun_id,da)

	conn.WriteData(f)
	return nil
}

//向充电桩下发停止充电命令
func (conn *HpCharge) ChargeStop(gun_id string,UserId string,Money int32,OrderId uint32) error{
	//var err error
	da := &StartStopPack{
		Marking:   2,
		UserId:    UserId,
		Money:     Money,
		OrderId:   OrderId,
		Mode:      1,
		Parameter: Money,
	}
	buf := new(bytes.Buffer)
	da.Pack(buf)

	f := PackAll(conn.DeviceId, "10", "0", gun_id, 0, buf.Bytes())

	//清空缓存 便于充电桩返回数据
	//data.Cache.Rm(fmt.Sprintf("charge_startstop_return_data_%d_%s",conn.DeviceId,gun_id))
	//data.Cache.Rm(fmt.Sprintf("charge_report_start_return_data_%d_%s",conn.DeviceId,gun_id))

	Log.Info("[%s][%d-%s][主动停止充电][%+v]",conn.ConnId,conn.DeviceId,gun_id,da)

	conn.WriteData(f)
	return nil
}
