/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10 10:17
 */
//计费模型测试数据
package data

type BillingTime struct {
	Title string //阶段名称
	//Time_slot string
	Time             string  //时间段 按照该字段递增排序
	Elty_price       float32 //电价
	Service_price    float32 //服务费
	Slot_type int8 //阶段标识
	Billing_model_id int   //计费模型id
	Stage_slot_id    int   //阶段模型-子阶段id
	Stage_id         int   //阶段模型id
}
type BtSlice []BillingTime

var billingData BtSlice

func init(){
	billingData=append(billingData,BillingTime{
		Title:"谷",Time:"00:00-05:00",Elty_price:0.41,Service_price:0.61,Slot_type:2,Billing_model_id:1,Stage_slot_id:2,Stage_id:1,

	})
	billingData=append(billingData,BillingTime{
		Title:"平",Time:"05:00-07:30",Elty_price:0.81,Service_price:0.61,Slot_type:3,Billing_model_id:1,Stage_slot_id:3,Stage_id:1,
	})

	billingData=append(billingData,BillingTime{
		Title:"峰",Time:"07:30-11:30",Elty_price:1.21,Service_price:0.61,Slot_type:1,Billing_model_id:1,Stage_slot_id:1,Stage_id:1,
	})
	billingData=append(billingData,BillingTime{
		Title:"平",Time:"11:30-17:00",Elty_price:0.81,Service_price:0.61,Slot_type:3,Billing_model_id:1,Stage_slot_id:3,Stage_id:1,
	})
	billingData=append(billingData,BillingTime{
		Title:"峰",Time:"17:00-18:00",Elty_price:1.21,Service_price:0.61,Slot_type:1,Billing_model_id:1,Stage_slot_id:1,Stage_id:1,
	})
	billingData=append(billingData,BillingTime{
		Title:"峰",Time:"18:00-19:00",Elty_price:1.21,Service_price:0.61,Slot_type:1,Billing_model_id:1,Stage_slot_id:1,Stage_id:1,
	})
	billingData=append(billingData,BillingTime{
		Title:"峰",Time:"19:00-21:00",Elty_price:1.21,Service_price:0.61,Slot_type:1,Billing_model_id:1,Stage_slot_id:1,Stage_id:1,
	})
	billingData=append(billingData,BillingTime{
		Title:"平",Time:"21:00-22:00",Elty_price:0.81,Service_price:0.61,Slot_type:3,Billing_model_id:1,Stage_slot_id:3,Stage_id:1,
	})
	billingData=append(billingData,BillingTime{
		Title:"谷",Time:"22:00-23:00",Elty_price:0.41,Service_price:0.61,Slot_type:2,Billing_model_id:1,Stage_slot_id:2,Stage_id:1,
	})
	billingData=append(billingData,BillingTime{
		Title:"谷",Time:"23:00",Elty_price:0.41,Service_price:0.61,Slot_type:2,Billing_model_id:1,Stage_slot_id:2,Stage_id:1,
	})
}

//获取计费模型数据并返回
func GetBillingData() *BtSlice {
	return &billingData
}
