/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10 10:17
 */
package base

//日志处理
import (
	"encoding/json"
	"github.com/astaxie/beego/logs"
)

type config struct {
	Filename string `json:"filename"`
	Maxlines int    `json:"maxlines"`
}

func NewLog() *logs.BeeLogger {
	l := logs.NewLogger(1000)
	c := config{
		Filename: C.ConfigPath.Log,
		Maxlines: 100000,
	}
	b, _ := json.Marshal(c)

	if C.Log.File{
		_ = l.SetLogger("file", string(b))
	}

	if C.Log.Console{
		_ = l.SetLogger("console", string(b))
	}

	if C.Log.Debug == true {
		l.SetLevel(logs.LevelDebug) //信息模式
		l.EnableFuncCallDepth(true)
		l.SetLogFuncCallDepth(2)
	} else {
		l.SetLevel(logs.LevelInfo) //信息模式
	}

	return l
}
