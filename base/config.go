/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10
 */
package base

import (
	"errors"
	"flag"
	"gopkg.in/gcfg.v1"
	"time"
)

type Config struct {
	Tcp struct {
		Listen string
	}
	Log struct{
		Debug bool
		File bool
		Console bool
	}
	Pile struct{
		List string
	}
	Charge struct{
		Money int
		GunId string
		Time time.Duration
	}
	ConfigPath struct {
		Path string //配置文件路径
		Log string //日志文件路径
	}
}

func newConfig() *Config {
	var c Config
	//默认路径
	c.ConfigPath.Path = getPath() + "configTest.ini"
	c.ConfigPath.Log = getPath() + `logs` + DIRSTR + `testLog.log`

	c.ParseCmdlineOpt()

	err := gcfg.ReadFileInto(&c, c.ConfigPath.Path)
	if err != nil {
		panic(errors.New("配置文件不存在!"))
	}
	return &c
}

func (c *Config) ParseCmdlineOpt() {
	flag.StringVar(&c.ConfigPath.Path, "c", c.ConfigPath.Path, "配置文件路径")
	flag.StringVar(&c.ConfigPath.Log, "log", c.ConfigPath.Log, "日志文件路径")
	flag.Parse()
}
