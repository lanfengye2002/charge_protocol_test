/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10 10:17
 */
package base

import(
	//"fmt"
	"os"
	"path/filepath"
	"runtime"
)
//获取当前文件所在路径
func getPath() string{
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	return exPath+DIRSTR
}

//获取系统路径分隔符
func getDirStr() string{
	var path string
	if runtime.GOOS == "windows"{
		path = "\\"
	}else if runtime.GOOS == "linux"{
		path = "/"
	}
	return  path
}


