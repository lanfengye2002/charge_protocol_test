/*
* @Author: 窦子滨<zibin_5257@163.com>
* @Date: 2019-04-10 10:17
 */
package base

//包初始化
var (
	DIRSTR string=getDirStr() //路径分隔符号
	//包全局配置
	C = newConfig()
	Log = NewLog()
)
